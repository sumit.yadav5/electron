const {alias} = require('react-app-rewire-alias')

module.exports = function override(config) {
  alias({
    '@atoms': 'src/Atoms',
    '@pages' : 'src/Pages'
  })(config)

  return config
}