  module: {
    rules: [
      {
        test: /\.css$/,
        include: [webpackPaths.srcRendererPath],
        use: ['style-loader', 'css-loader', 'postcss-loader'],
      }
    ]
  }

