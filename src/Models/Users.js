import { User } from "./User";

export class UserList extends Array {
    constructor(users = []) {
        if (users.length) {
            super(...User.fromList(users))
        } else {
            super()
        }
    }
    addUser(user) {
        this.push(new User(user))
    }

    findIndexById(id) {
        return this.findIndex((user) => user.id === id)
    }

    updateUser(updateUserData) {
        const index = this.findIndexById(updateUserData.id)
        this[index].update(updateUserData)
    }
    
    deleteUser(id) {
        const index = this.findIndexById(id)
        this.splice(index, 1)
    }
}

