export class User {
    constructor(user = {}) {
        this.id = user.id;
        this.name = user.name;
        this.username = user.username;
        this.email = user.email;
        this.city = user.city;
    }

    get displayName() {
        return this.name.toUpperCase()
    }

    update(params = {}) {
        Object.assign(this, params)
    }

    static fromList(users = []) {
        return users.map(user => new User(user));
    }

}