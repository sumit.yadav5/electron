import {  makeStyles } from '@material-ui/core/styles';
export const headCells = [
  {
    id:'id',
    disablePadding: true,
    label:'ID'
  },
  {
    id:'name',
    disablePadding: true,
    label:'Name'
  },
  {
    id:'userName',
    disablePadding: true,
    label:'UserName'
  },
  {
    id:'email',
    disablePadding: true,
    label:'Email'
  },
  {
    id:'city',
    disablePadding: true,
    label:'City',
  },
  {
    id:'action',
    disablePadding: true,
    label:'Action',
  }
]  
export const useStyles = makeStyles((theme) => ({
    root: {
      margin:'20px'
    },
    paper: {
      width: '100%',
      marginBottom: theme.spacing(2),
    },
    table: {
      minWidth: 750,
    },
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  }));