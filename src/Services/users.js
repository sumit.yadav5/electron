import axios from "axios";
export default class UserService {

    static getAllUsers() {
        return axios.get(`${process.env.REACT_APP_BASE_URL}/users`)
    }

    static setUser(user) {
        return axios.post(`${process.env.REACT_APP_BASE_URL}/users`, user )
    }

    static updateUser(user) {
        const {id} = user
        return axios.put(`${process.env.REACT_APP_BASE_URL}/users/${id}`, user)
    }

    static deleteUser(id) {
        return axios.delete(`${process.env.REACT_APP_BASE_URL}/users/${id}`)
    }
}