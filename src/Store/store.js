import { configureStore,  combineReducers  } from "@reduxjs/toolkit";
import { persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import users from './reducers/usersSlice'

const persistConfig = {
    key: 'root',
    storage: storage,
  };

const reducer  = combineReducers({
    users
});

const persistedReducer = persistReducer(persistConfig, reducer);
const store = configureStore({
    reducer: persistedReducer
});

export default store;