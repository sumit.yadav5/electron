import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import UserService from "../../Services/users";
import { UserList } from "../../Models/Users";

export const initialState = {
    users: new UserList()
};

export const getUsers = createAsyncThunk("users/list", async () => {
    try{
        const response = await UserService.getAllUsers();
        return response.data;
    }catch(error){
        console.log("error", error)
    }
});

export const setUser = createAsyncThunk("set/user", async (user) => {
     try{
       const response = await UserService.setUser(user);
       return response.data;
     }catch(error){
        console.log("error", error)
     }
});

export const updateUserData = createAsyncThunk("update/user", async (updateUser) => {
      try{
        const response = await UserService.updateUser(updateUser);
        return response.data;
      }catch(error){
         console.log("error", error)
      }
})

export const deletedUser = createAsyncThunk("deleted/user", async (id) => {
    try{
      await UserService.deleteUser(id);
      return id;
    }catch(error){
       console.log("error", error)
    }
})
 
export const usersSlice  = createSlice({
    name:'users',
    initialState,
    reducers:{},
    extraReducers: {
        [getUsers.fulfilled.type]: (state, action) => {
            state.users = new UserList(action.payload);
        },
        [setUser.fulfilled.type]:(state, action) => {
            state.users.addUser(action.payload)
        },
        [updateUserData.fulfilled.type]:(state, action) => {
            state.users.updateUser(action.payload);
        },
        [deletedUser.fulfilled.type]:(state, id) => {
            state.users.deleteUser(id);
        }
    }
});

export default usersSlice.reducer;
