import React from 'react'
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import TableCell from '@material-ui/core/TableCell';
import { headCells } from '../../utils/Utils';

const EnhancedTableHead = () => {
  return (
    <TableHead>
    <TableRow>
      {headCells.map((headCell) => (
        <TableCell
          key={headCell.id}
          align={headCell.numeric ? 'right' : 'left'}
          padding="normal"
        >
            {headCell.label}
        </TableCell>
      ))}
    </TableRow>
  </TableHead>
  )
}

export default EnhancedTableHead