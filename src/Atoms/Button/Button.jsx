import React from 'react'

const Button = ({type="button", children="Submit"}) => {
  return (
    <button type={type} className='button-style'>{children}</button>
  )
}

export default Button
