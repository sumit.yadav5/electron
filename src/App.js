import './App.css';
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Dashboard from '@pages/Dashboard';
import EditForm from '@pages/EditForm';
import AddForm from '@pages/AddForm';

function App() {
  return (
    <div className="App">
      <Router>
         <Routes>
            <Route path='/' element={<Dashboard />}/>
            <Route path='/add-form' element={<AddForm />} />
            <Route path='/edit-form/:id' element={<EditForm />} />
         </Routes>
      </Router>
    </div>
  );
}

export default App;
