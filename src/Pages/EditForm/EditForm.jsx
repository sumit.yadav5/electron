import React, { useState } from 'react'
import { useNavigate, useParams  } from 'react-router-dom'
import Button from '@atoms/Button'
import { useDispatch, useSelector } from "react-redux";
import  { updateUserData }  from '../../Store/reducers/usersSlice';



const EditForm = () => {
  let { id } = useParams();
  const navigate = useNavigate()
  const dispatch = useDispatch();
  const {users} = useSelector((state) => state.users);
  const existingUser = users?.filter((userData) => userData.id === id)
  const {id:userId, name, username, email, city} = existingUser[0]
  const redirectToDashboard = () => {
        navigate('/')
  }
  const [updateUser, setUpdateUser] = useState({
    id:userId,
    name,
    username,
    email,
    city
  });

  const handelChange = (e) => {
    const value = e.target.value
    setUpdateUser({
      ...updateUser,
      [e.target.name]:value,
    }) 
  }

  const submitHandler = (e) => {
     e.preventDefault();
     dispatch(updateUserData(updateUser))
     setUpdateUser({
      id:'',
      name:'',
      username:'',
      email:'',
      city:''
     })
     redirectToDashboard();
  }
  return (
    <div>
      <div className='main-heading sub-heading-align-right' onClick={redirectToDashboard}>Go to Dashboard</div>
      <h2 className='main-heading'>Update details page</h2>
      <form className='form-style-box' onSubmit={submitHandler}>
        <input type="text" placeholder='Please Enter ID' name="id" value={updateUser.id} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your Name' name="name" value={updateUser.name} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your UserName' name="username" value={updateUser.username} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your Email' name="email" value={updateUser.email} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your City' name="city" value={updateUser.city} onChange={handelChange} className='input-type-style'/>
        <Button type='submit'>update</Button>
      </form>
     </div>
  )
}

export default EditForm
