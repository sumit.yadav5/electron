import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useStyles } from "../../utils/Utils";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import EnhancedTableHead from "../../Components/EnhancedTableHead/EnhancedTableHead";
import { getUsers, deletedUser } from "../../Store/reducers/usersSlice";

import { useNavigate } from "react-router-dom";

const Dashboard = () => {
  const classes = useStyles();
  const { users } = useSelector((state) => state.users);
  const dispatch = useDispatch();
  const navigate = useNavigate();

  const redirectToEditForm = (id) => {
    navigate(`/edit-form/${id}`);
  };
  const redirectToAddForm = () => {
    navigate("/add-form");
  };
  const deleteUserHandler = (id) => {
    dispatch(deletedUser(id));
  };

  useEffect(() => {
    dispatch(getUsers());
  }, []);
  return (
    <div className="dashboard-container">
      <button className="add-user-style" onClick={redirectToAddForm}>
        Add User
      </button>
      <h1 className="dashboard-heading text-bold font-bold underline">This is Dashboard page</h1>
      <div className={classes.root}>
        <Paper className={classes.paper}>
          <TableContainer>
            <Table
              className={classes.table}
              aria-labelledby="tableTitle"
              size={"medium"}
              aria-label="enhanced table"
            >
              <EnhancedTableHead classes={classes} rowCount={users?.length} />
              <TableBody>
                {users?.map((user) => {
                  return (
                    <TableRow
                      hover
                      role="checkbox"
                      tabIndex={-1}
                      key={user?.id}
                    >
                      <TableCell align="left">{user?.id}</TableCell>
                      <TableCell component="th" scope="row" padding="normal">
                        {user?.displayName}
                      </TableCell>
                      <TableCell align="left">{user?.username}</TableCell>
                      <TableCell align="left">{user?.email}</TableCell>
                      <TableCell align="left">{user?.city}</TableCell>
                      <TableCell align="left">
                        <span
                          className="edit-btn"
                          onClick={() => redirectToEditForm(user.id)}
                        >
                          <a href="#">Edit</a>
                        </span>
                        <span
                          className="delete-btn"
                          onClick={() => deleteUserHandler(user.id)}
                        >
                          <a href="#">Delete</a>
                        </span>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Paper>
      </div>
    </div>
  );
};

export default Dashboard;
