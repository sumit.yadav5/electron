import React, { useState } from 'react'
import { useNavigate } from 'react-router-dom'
import Button from '@atoms/Button'
import { useDispatch } from "react-redux";
import  { setUser }  from '../../Store/reducers/usersSlice';

const AddForm = () => {
  const navigate = useNavigate()
  const dispatch = useDispatch();

  const [users, setUsers] = useState({
    id:'',
    name:'',
    username:'',
    email:'',
    city:''
  });

  const handelChange = (e) => {
    const value = e.target.value
    setUsers({
      ...users,
      [e.target.name]:value,
    }) 
  }

  const redirectToDashboard = () => {
    navigate('/')
}

  const submitHandler = (e) => {
     e.preventDefault();
     dispatch(setUser(users))
     setUsers({
      id: null,
      name:'',
      username:'',
      email:'',
      city:''
     })
     redirectToDashboard()
  }


  return (
    <div>
      <div className='main-heading sub-heading-align-right' onClick={redirectToDashboard}>Go to Dashboard</div>
      <h2 className='main-heading'>Add user details page</h2>
      <form className='form-style-box' onSubmit={submitHandler} >
        <input type="text" placeholder='Please Enter ID' name="id" value={users.id} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your Name' name="name" value={users.name} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your UserName' name="username" value={users.username} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your Email' name="email" value={users.email} onChange={handelChange} className='input-type-style'/>
        <input type="text" placeholder='Please Enter Your City' name="city" value={users.city} onChange={handelChange} className='input-type-style'/>
        <Button type='submit'>Submit</Button>
      </form>
     </div>
  )
}

export default AddForm
